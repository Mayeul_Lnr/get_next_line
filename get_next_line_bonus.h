/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 14:46:40 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 00:39:59 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

# include <stdlib.h>
# include <unistd.h>

typedef struct s_stor
{
	int				fd;
	char			*buff;
	struct s_stor	*next;
}	t_stor;

int		get_next_line(int fd, char **line);

int		buffer_len(char *s);

char	*buffer_join(char *buffer, char *line);

int		flush_buffer(char *buffer, char **line);

char	*storage_find(t_stor **storage, int fd);

void	storage_clean(t_stor **storage, int fd);

#endif
