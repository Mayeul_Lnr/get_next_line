/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 15:56:48 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 00:38:01 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	panic_free(char **s)
{
	free(*s);
	*s = NULL;
	return (-1);
}

int	get_next_line(int fd, char **line)
{
	static char	buff[BUFFER_SIZE + 2] = {0};
	int			n;

	if (!line)
		return (-1);
	*line = malloc(1);
	if (!*line)
		return (-2);
	**line = '\0';
	if (fd < 0 || BUFFER_SIZE < 1)
		return (panic_free(line));
	n = 1;
	while (!flush_buffer(buff, line) && n)
	{
		if (!*line)
			return (panic_free(line));
		n = read(fd, buff, BUFFER_SIZE);
		if (n < 0)
			return (panic_free(line));
		buff[n] = '\0';
	}
	return ((n > 0) - (n == -1));
}
