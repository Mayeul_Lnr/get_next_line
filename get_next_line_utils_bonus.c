/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils_bonus.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/27 15:19:29 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 01:05:16 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

int	buffer_len(char *s)
{
	size_t	len;

	if (!s)
		return (0);
	len = 0;
	while (s[len] && s[len] != '\n')
		len++;
	return (len);
}

char	*buffer_join(char *buffer, char *line)
{
	size_t	i;
	size_t	j;
	char	*ret;

	i = buffer_len(buffer) + buffer_len(line);
	ret = malloc(i + 1);
	if (!ret)
		return (NULL);
	i = 0;
	while (line[i])
	{
		ret[i] = line[i];
		i++;
	}
	j = 0;
	while (buffer[j] && buffer[j] != '\n')
	{
		ret[i] = buffer[j];
		i++;
		j++;
	}
	ret[i] = '\0';
	return (ret);
}

int	flush_buffer(char *buffer, char **line)
{
	size_t	i;
	size_t	j;
	int		ret;
	char	*temp;

	temp = buffer_join(buffer, *line);
	if (!temp)
	{
		free(*line);
		*line = NULL;
		return (0);
	}
	free(*line);
	*line = temp;
	j = 0;
	while (buffer[j] && buffer[j] != '\n')
		j++;
	ret = (buffer[j] == '\n');
	i = 0;
	while (buffer[j])
		buffer[i++] = buffer[++j];
	return (ret);
}

char	*storage_find(t_stor **storage, int fd)
{
	if (!*storage)
	{
		*storage = malloc(sizeof(t_stor));
		if (!*storage)
			return (NULL);
		(*storage)->buff = malloc(BUFFER_SIZE + 1);
		if (!(*storage)->buff)
		{
			free(*storage);
			return (NULL);
		}
		((*storage)->buff)[0] = '\0';
		(*storage)->fd = fd;
		(*storage)->next = NULL;
		return ((*storage)->buff);
	}
	else if ((*storage)->fd == fd)
		return ((*storage)->buff);
	else
		return (storage_find(&((*storage)->next), fd));
}

void	storage_clean(t_stor **storage, int fd)
{
	t_stor	*temp;

	if (!*storage)
		return ;
	if ((*storage)->fd == fd || fd == -1)
	{
		temp = (*storage)->next;
		free((*storage)->buff);
		free(*storage);
		*storage = temp;
		storage_clean(storage, fd);
	}
	else
		storage_clean(&((*storage)->next), fd);
}
