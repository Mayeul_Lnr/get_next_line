/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 15:56:48 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 00:59:25 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

static int	panic_free(char **line, int ret)
{
	free(*line);
	*line = NULL;
	return (ret);
}

static t_stor	*ft_fnv3(t_stor **stor, int fd, char **line)
{
	if (!line)
		return (NULL);
	*line = malloc(1);
	if (!*line)
		return (NULL);
	if (fd < 0 || BUFFER_SIZE < 1)
		return (NULL);
	**line = '\0';
	return (storage_find(&stor, fd));
}

int	get_next_line(int fd, char **line)
{
	static t_stor	*stor = NULL;
	char			*buff;
	int				n;

	buff = ft_fnv3(&stor, fd, line);
	if (!buff)
		return (panic_free(line, -1));
	n = 1;
	while (!flush_buffer(buff, line) && n)
	{
		if (!*line)
			n = read(fd, buff, BUFFER_SIZE);
		if (!*line || n < 0)
		{
			storage_clean(&stor, (*line != 0) * fd - (!*line));
			return (panic_free(line, -2));
		}
		buff[n] = '\0';
	}
	if (n < 1)
		storage_clean(&stor, fd);
	return ((n > 0) - (n == -1));
}
