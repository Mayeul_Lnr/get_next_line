/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/27 15:19:29 by mlaneyri          #+#    #+#             */
/*   Updated: 2020/12/31 01:02:13 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int	buffer_len(char *s)
{
	size_t	len;

	if (!s)
		return (0);
	len = 0;
	while (s[len] && s[len] != '\n')
		len++;
	return (len);
}

char	*buffer_join(char *buffer, char *line)
{
	size_t	i;
	size_t	j;
	char	*ret;

	i = buffer_len(buffer) + buffer_len(line);
	ret = malloc(i + 1);
	if (!ret)
		return (NULL);
	i = 0;
	while (line[i])
	{
		ret[i] = line[i];
		i++;
	}
	j = 0;
	while (buffer[j] && buffer[j] != '\n')
	{
		ret[i] = buffer[j];
		i++;
		j++;
	}
	ret[i] = '\0';
	return (ret);
}

int	flush_buffer(char *buffer, char **line)
{
	size_t	i;
	size_t	j;
	int		ret;
	char	*temp;

	temp = buffer_join(buffer, *line);
	if (!temp)
	{
		free(*line);
		*line = NULL;
		return (0);
	}
	free(*line);
	*line = temp;
	j = 0;
	while (buffer[j] && buffer[j] != '\n')
		j++;
	ret = (buffer[j] == '\n');
	i = 0;
	while (buffer[j])
		buffer[i++] = buffer[++j];
	return (ret);
}
